﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CNSSwpf.Login
{
    class LoginVM : INotifyPropertyChanged
    {
        string connectionString = "DATA SOURCE = localhost:1521 / XE; USER ID = DB_CNSS;Password=1252;";
        private string _login;
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                RaisePropertyChanged("Login");
            }
        }
        private string _role;
        public string Role
        {
            get { return _role; }
            set
            {
                _role = value;
                RaisePropertyChanged("Role");
            }
        }

        public IEnumerable<string> Roles
        {
            get
            {
                return new List<string> { "Agent de Bcrv", "Agent de Guichet", "Agent de service", "Medecin","Administrateur" };
            }
        }

        private ICommand _loginCommand;

        public ICommand LoginCommand
        {
            get { return _loginCommand; }
            set
            {
                _loginCommand = value;
                RaisePropertyChanged("LoginCommand");
            }
        }
        private ICommand _resetCommand;

        public ICommand ResetCommand
        {
            get { return _resetCommand; }
            set
            {
                _resetCommand = value;
                RaisePropertyChanged("ResetCommand");
            }
        }
        private string _lbl_loginState;
        public string lbl_loginState
        {
            get { return _lbl_loginState; }
            set
            {
                _lbl_loginState = value;
                RaisePropertyChanged("lbl_loginState");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public LoginVM()
        {
            LoginCommand = new RelayCommand(o => loginAction(o), o => canLogin(o));
            ResetCommand = new RelayCommand(o => resetAction(o));
        }

        private void loginAction(object parameter)
        {
<<<<<<< HEAD
<<<<<<< HEAD
            OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;
            OracleCommand cmd = new OracleCommand();
            var password = (parameter as PasswordBox).Password;
            cmd.CommandText = "SELECT * from LOGIN where LOGIN ='Mariem' and PASSWORD ='1252'";
            //cmd.CommandText = "select * from LOGIN where LOGIN ='"+Login+"' and PASSWORD ='"+ password + "'";
            con.Open();
            cmd.Connection = con;
            cmd.BindByName = true;
            cmd.CommandType = System.Data.CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            if(dr.HasRows)
            {
                if (dr.GetString(2)== "AgentBCRV")
                {
                    Window1 w1 = new Window1();
                    Application.Current.MainWindow.Hide();
                    w1.ShowDialog();
                    Application.Current.MainWindow.Close();

                }
                else
                {
                    if (dr.GetString(2) == "AgentGuichet")
                    {
                        SaisieRdv.SaisieRdv saisieRDV= new SaisieRdv.SaisieRdv();
                        Application.Current.MainWindow.Hide();
                        saisieRDV.ShowDialog();
                        Application.Current.MainWindow.Close();

                    }
                }
=======

            try
            {
>>>>>>> 134f1bd49655fbf41e2b2f77f006f5f0a3477525

=======

            try
            {

>>>>>>> 134f1bd49655fbf41e2b2f77f006f5f0a3477525
                OracleConnection con = new OracleConnection();
                con.ConnectionString = connectionString;
                OracleCommand cmd = new OracleCommand();
                var password = (parameter as PasswordBox).Password;
                cmd.CommandText = "select * from LOGIN where LOGIN ='Mariem' and PASSWORD ='1252'";
                //cmd.CommandText = "select * from LOGIN where LOGIN ='" + Login + "' and PASSWORD ='" + password + "' + and Role='" + Role + "'";
                con.Open();
                cmd.Connection = con;
                cmd.BindByName = true;
                cmd.CommandType = System.Data.CommandType.Text;
                OracleDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    Window1 w1 = new Window1();
                    Application.Current.MainWindow.Hide();
                    w1.ShowDialog();
                    Application.Current.MainWindow.Close();
                }
            }
            catch (Exception e)
            {

                MessageBox.Show("Verifié vos paramétres");
                MessageBox.Show(Role);
            }

            
        }

        private bool canLogin(object parameter)
        {
            var password = (parameter as PasswordBox).Password;
            if (string.IsNullOrEmpty(Role) || string.IsNullOrEmpty(Login) || string.IsNullOrEmpty(password) )
            {
                return false;
            }
            return true;
        } 

        private void resetAction(object parameter)
        {
            Login = "";
            var password = parameter as PasswordBox;
            password.Password = "";
        }


        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
