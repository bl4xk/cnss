﻿using CNSSwpf.Models;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;

namespace CNSSwpf.MedecinPreinscripeur
{
    class MedecinPreinscripeurVM : INotifyPropertyChanged
    {
                   
        #region attribute
        private RelayCommand _save;

        private string _numCode;
        public string NumCode
        {
            get { return _numCode; }
            set
            {
                _numCode = value;
                RaisePropertyChanged("NumCode");
            }
        }
        
        private string _nomMedecin;
        public string NomMedecin
        {
            get { return _nomMedecin; }
            set
            {
                _nomMedecin = value;
                RaisePropertyChanged("NomMedecin");
            }
        }
        
        private string _specialité;
        public string Specialité
        {
            get { return _specialité; }
            set
            {
                _specialité = value;
                RaisePropertyChanged("Specialité");
            }
        }

        private string _adresse;
        public string Adresse
        {
            get { return _adresse; }
            set
            {
                _adresse = value;
                RaisePropertyChanged("Adresse");
            }
        }
        private int _cp;
        public int Cp
        {
            get { return _cp; }
            set
            {
                _cp = value;
                RaisePropertyChanged("CP");
            }
        }
        private string _ville;
        public string Ville
        {
            get { return _ville; }
            set
            {
                _ville = value;
                RaisePropertyChanged("Ville");
            }
        }
        private string _tel;
        public string Tel
        {
            get { return _tel; }
            set
            {
                _tel = value;
                RaisePropertyChanged("Tel");
            }
        }

        private string _fax;
        public string Fax
        {
            get { return _fax; }
            set
            {
                _fax = value;
                RaisePropertyChanged("Fax");
            }
        }
        
        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged("Email");
            }
        }
       
        
        public RelayCommand Save
        {
            get { return _save; }
            set { _save = value; }
        }
        #endregion attribute
        
        string connectionString = "DATA SOURCE = localhost:1521 / XE; USER ID = DB_CNSS;Password=1252;";

        public MedecinPreinscripeurVM()
        {
            Save = new RelayCommand(o => SaveAction(o));
        }

        private void SaveAction(object parameter)
        {
            OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;

            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            OracleCommand loCmd = new OracleCommand();
            con.Open();

            try
            {
                cmd.CommandText = " INSERT INTO MEDECIN VALUES ('"+ NumCode+"', '"+ NomMedecin + "','"+ Specialité + "', '" + Adresse + "', '" + Cp + "' ,'" + Ville + "','" + Tel+"','"+Fax+"','"+Email+ "')";
                int rowsUpdated = cmd.ExecuteNonQuery();
                if (rowsUpdated == 0)
                    MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    MessageBox.Show("Ajout Réussi!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                con.Dispose();

            }
            catch (OracleException ex)
            {
                MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        



        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        
    }
}
