﻿using CNSSwpf.Models;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;

namespace CNSSwpf.Correspondant
{
    class CorrespondantVM : INotifyPropertyChanged
    {
        #region Attribut

        private string _correspondant;
        private string _designation;
        private string _regroupement;
        private string _idfiscal;
        private string _adresse;
        private string _ville;
        private string _cp_ville;
        private string _tel;
        private string _fax;
        private string _email;
        private RelayCommand _save;
        [Required(ErrorMessage = "ce champ est obligatoire")]
        public string Correspondant
        {
            get { return _correspondant; }
            set {
                _correspondant = value;
                RaisePropertyChanged("Correspondant");
            }
        }

        public string Designation
        {
            get { return _designation; }
            set
            {
                _designation = value;
                RaisePropertyChanged("Designation");
            }
        }
        
        public string Regroupement
        {
            get { return _regroupement; }
            set
            {
                _regroupement = value;
                RaisePropertyChanged("Regroupement");
            }
        }

        public string Idfiscal
        {
            get { return _idfiscal; }
            set
            {
                _idfiscal = value;
                RaisePropertyChanged("Idfiscal");
            }
        }
        
        public string Adresse
        {
            get { return _adresse; }
            set
            {
                _adresse = value;
                RaisePropertyChanged("Adresse");
            }
        }
        public string Ville
        {
            get { return _ville; }
            set
            {
                _ville = value;
                RaisePropertyChanged("Ville");
            }
        }
        public string CP_Ville
        {
            get { return _adresse; }
            set
            {
                _cp_ville = value;
                RaisePropertyChanged("CP_Ville");
            }
        }

       
        public string Tel
        {
            get { return _tel; }
            set
            {
                _tel = value;
                RaisePropertyChanged("Tel");
            }
        }
        
        public string Fax
        {
            get { return _fax; }
            set
            {
                _fax = value;
                RaisePropertyChanged("Fax");
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged("Email");
            }
        }


     
        public RelayCommand Save
        {
            get { return _save; }
            set { _save = value; }
        }
        #endregion
        string connectionString = "DATA SOURCE = localhost:1521 / XE; USER ID = DB_CNSS;Password=1252;";

        public CorrespondantVM()
        {
            Save = new RelayCommand(o => SaveAction(o));
        }
        private void SaveAction(object parameter)
        {

            OracleConnection con = new OracleConnection();

            con.ConnectionString = connectionString;
            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            con.Open();

            try {
                cmd.CommandText = " INSERT INTO CORRESPONDANT(ID, DESIGNATION, REGROUPEMENT, FISCALID, ADRESSE, TEL, FAX, EMAIL, VILLE, CP_VILLE)" +
                "VALUES (SEQUENCE_ID.NEXTVAL,'" + Designation + "', '" + Regroupement + "', '" + Idfiscal + "', '" + Adresse + "', '" + Tel + "', '" + Fax + "', '" + Email + "','" + Ville + "','" + CP_Ville + "')";
            int rowsUpdated = cmd.ExecuteNonQuery();
            if (rowsUpdated == 0)
                    MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    MessageBox.Show("Ajout Réussi!");
                con.Dispose();

            }
            catch (OracleException ex)
            {
               MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
          
        }

        //LoginCommand = new RelayCommand(o => loginAction(o));
        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
