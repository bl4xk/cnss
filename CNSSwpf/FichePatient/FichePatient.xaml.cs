﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CNSSwpf.FichePatient
{
    /// <summary>
    /// Interaction logic for FichePatient.xaml
    /// </summary>
    public partial class FichePatient : Window
    {
        public FichePatient()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Correspondant.CorrespandontView f = new Correspondant.CorrespandontView();
            f.ShowDialog();

        }
    }
}
