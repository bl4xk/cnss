﻿using CNSSwpf.Models;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;

namespace CNSSwpf.FichePatient
{
    class FichePatientVM : INotifyPropertyChanged
    {


        #region attribute

        private string _matricule;
        public string Matricule
        {
            get { return _matricule; }
            set
            {
                _matricule = value;
                RaisePropertyChanged("Matricule");
            }
        }
        private string _nomAdérent;
        public string NomAdérent
        {
            get { return _nomAdérent; }
            set
            {
                _nomAdérent = value;
                RaisePropertyChanged("NomAdérent");
            }
        }

        private string _prénomAdérent;
        public string PréNomAdérent
        {
            get { return _prénomAdérent; }
            set
            {
                _prénomAdérent = value;
                RaisePropertyChanged("PréNomAdérent");
            }
        }

        private string _address;
        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                RaisePropertyChanged("Address");
            }
        }

        private string _dateNaissance;
        public string DateNaissance
        {
            get { return _dateNaissance; }
            set
            {
                _dateNaissance = value;
                RaisePropertyChanged("DateNaissance");
            }
        }
        private string _tel;

        public string Tel
        {
            get { return _tel; }
            set
            {
                _tel = value;
                RaisePropertyChanged("Tel");
            }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set {
                _email = value;
                RaisePropertyChanged("Email");
                }
        }


        private string _ville;

        public string Ville
        {
            get { return _ville; }
            set
            {
                _ville = value;
                RaisePropertyChanged("Ville");
            }
        }
        private int _codepostale;

        public int CodePostale
        {
            get { return _codepostale; }
            set
            {
                _codepostale = value;
                RaisePropertyChanged("CodePostale");
            }
        }

        private string _genre;
        public string Genre
        {
            get { return _genre; }
            set
            {
                _ville = value;
                RaisePropertyChanged("Genre");
            }
        }



        #endregion attribute
        
        private void SaveAction(object parameter)
        {
<<<<<<< HEAD
<<<<<<< HEAD
              OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;

            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            OracleCommand loCmd = new OracleCommand();
            con.Open();

            try
            {
                cmd.CommandText = "INSERT INTO CORRESPONDANT('" + Address + "', '" + Cpville + "', " + Tel + "','" + Profession + "' , '" + Taille + "' , " + Poids + "', '" + Categorie + "', '" + SousCategorie + "', " + AnsRecrutement + "','" + DateHospitalisation + "' , '" + DureeValiditéSupport + "','" + Qualité + "' , '" + NomAdérent + "','" + PréNomAdérent + "' , '" + RefPiéce + "')";

                int rowsUpdated = cmd.ExecuteNonQuery();
                if (rowsUpdated == 0)
                    MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    MessageBox.Show("Ajout Réussi!");
                con.Dispose();

            }
            catch (OracleException e)
            {
                MessageBox.Show("Veuiilez Remplir touts les champs :  "+ e , "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

=======
            Person Prs = new Person(Matricule,NomAdérent, Address,DateNaissance,Tel,Email,Ville,CodePostale,Genre);
>>>>>>> 134f1bd49655fbf41e2b2f77f006f5f0a3477525
=======
            Person Prs = new Person(Matricule,NomAdérent, Address,DateNaissance,Tel,Email,Ville,CodePostale,Genre);
>>>>>>> 134f1bd49655fbf41e2b2f77f006f5f0a3477525

        }
        

        private RelayCommand _save;
        public FichePatientVM()
        {
            save = new RelayCommand(o => SaveAction(o));
        }


        public RelayCommand save
        {
            get { return _save; }
            set { _save = value; }
        }

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
