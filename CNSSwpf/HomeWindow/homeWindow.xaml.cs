﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CNSSwpf
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            fillCorrespondantBox();
        }
        public string connectionString = "DATA SOURCE = localhost:1521 / XE; USER ID = DB_CNSS;Password=1252;";

        public void fillCorrespondantBox()
        {
            comboboxCorrespondant.Items.Clear(); ;
            OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;
            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            con.Open();

            cmd.CommandText = "SElect * from CORRESPONDANT";
            cmd.Connection = con;
            cmd.BindByName = true;
            cmd.CommandType = System.Data.CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string Getcorrespondant = dr.GetString(1);
                comboboxCorrespondant.Items.Add(Getcorrespondant);
            }

            con.Dispose();


        }
        public void fillMedecinBox()
        {
            medecin.Items.Clear(); ;
            OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;
            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            con.Open();

            cmd.CommandText = "SElect * from medecin";
            cmd.Connection = con;
            cmd.BindByName = true;
            cmd.CommandType = System.Data.CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string GetMedecin = dr.GetString(1);
                medecin.Items.Add(GetMedecin);
            }

            con.Dispose();


        }
        private void textBox_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void textBlock_TextChanged(object sender, TextChangedEventArgs e)
        {
        
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime localDate = DateTime.Now;
            string day = localDate.Day.ToString();
            string month = localDate.Month.ToString();
            string Rawyear = localDate.Year.ToString();

            if (day.Length == 1)
            {
                day = "0" + day;
            }
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            string year = Rawyear[2].ToString() + Rawyear[3].ToString();
            txbDate.Text = day + month + year;
            txb_ordn.Text = localDate.ToString();

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Correspondant.CorrespandontView Corres = new Correspondant.CorrespandontView();
            Corres.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            FichePatient.FichePatient fp = new FichePatient.FichePatient();
            fp.ShowDialog();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MedecinPreinscripeur.MedecinPreinscripeur md = new MedecinPreinscripeur.MedecinPreinscripeur();
            md.ShowDialog();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            SaisieRdv.SaisieRdv sr = new SaisieRdv.SaisieRdv();
            sr.ShowDialog();

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            fillMedecinBox();

        }

        private void comboboxCorrespondant_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IdtextBox.Text = "";
            OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;
            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            con.Open();
            cmd.CommandText = "SElect * from CORRESPONDANT where Designation = '" + comboboxCorrespondant.SelectedItem.ToString()+ "' ";
            cmd.Connection = con;
            cmd.BindByName = true;
            cmd.CommandType = System.Data.CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();

               // MessageBox.Show(dr.GetString(0));

                //string GetIDcorrespondant = dr.GetString(0).ToString();
                //IdtextBox.Text = GetIDcorrespondant.ToString();
         

            con.Dispose();
        }

        private void medecin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OracleConnection con = new OracleConnection();
            con.ConnectionString = connectionString;
            OracleCommand cmd = new OracleCommand();
            cmd = con.CreateCommand();
            con.Open();
            cmd.CommandText = "SElect * from medecin where Nom = '" + medecin.SelectedItem.ToString() + "' ";
            cmd.Connection = con;
            cmd.BindByName = true;
            cmd.CommandType = System.Data.CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            con.Dispose();


        }
    }

    internal class HomeWindowVM
    {
        internal void fillCorrespondantBox()
        {
            throw new NotImplementedException();
        }
    }
}
