﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CNSSwpf.Models
{
    class Horaire
    {
        public string lundi = "";
        public string mardi = "";
        public string mercredi = "";
        public string jeudi = "";
        public string vendredi = "";
        public string samedi = "";
        public string dimanche = "";
        private int _idHoraire;

        public int IdHoraire
        {
            get { return _idHoraire; }
            set { _idHoraire = value; }
        }
        private string _semaine;

        public string Semaine
        {
            get { return _semaine; }
            set { _semaine = value; }
        }
        public string semaine(params int[] list)    
        {
            return (string.Join("", list.ToArray()));

            /* string lundi = list[0].ToString();
             string mardi = list[1].ToString();
             string mercredi = list[2].ToString();
             string jeudi = list[3].ToString();
             string vendredi  = list[4].ToString();
             string samedi = list[5].ToString();
             string dimanche = list[6].ToString();
             string sem = lundi+mardi+mercredi+jeudi+vendredi+samedi+dimanche;*/

        }
        public bool insertHoraire()
        {
            string sem = this.semaine();
            DatabaseConnection db = new DatabaseConnection();
            db.connect();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText = "INSERT INTO horaire(SEQUENCE_ID.NEXTVAL,'" +  + "')";
            int rowsUpdated = cmd.ExecuteNonQuery();
            if (rowsUpdated == 0)
            {
                MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;

            }
            else
            {
                MessageBox.Show("Ajout Réussi!");
                return true;
            }


        }
    }
}
