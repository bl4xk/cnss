﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNSSwpf.Models
{
    class Correspondant
    {

        #region Attribut

        private string _correspondant;

        public string CorrespondantVar
        {
            get { return _correspondant; }
            set
            {
                _correspondant = value;
            }
        }

        private string _designation;
        public string Designation
        {
            get { return _designation; }
            set
            {
                _designation = value;
            }
        }

        private string _regroupement;
        public string Regroupement
        {
            get { return _regroupement; }
            set
            {
                _regroupement = value;
            }
        }

        private string _idfiscal;
        public string Idfiscal
        {
            get { return _idfiscal; }
            set
            {
                _idfiscal = value;
            }
        }

        private string _address;
        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
            }
        }

        private string _ville;
        public string Ville
        {
            get { return _ville; }
            set
            {
                _ville = value;
            }
        }

        private string _cp_ville;
        public string CP_Ville
        {
            get { return _cp_ville; }
            set
            {
                _cp_ville = value;
            }
        }

        private string _tel;
        public string Tel
        {
            get { return _tel; }
            set
            {
                _tel = value;
            }
        }

        private string _fax;
        public string Fax
        {
            get { return _fax; }
            set
            {
                _fax = value;
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
            }
        }



        #endregion


    }

}
