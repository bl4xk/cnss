﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;
using CNSSwpf.Login;

namespace CNSSwpf
{
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoginView_Loaded(object sender, RoutedEventArgs e)
        {
            LoginView.DataContext =  new LoginVM();
        }
    }
}
