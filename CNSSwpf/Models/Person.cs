﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace CNSSwpf.Models
{
    class Person
    {
        #region attribute
        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _matricule;
        public string Matricule
        {
            get { return _matricule; }
            set { _matricule = value; }
        }

        private string _nom;
        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        private string _prenom;
        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }

        private string  _address;
        public string  Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private string _dateNaissance;
        public string DateNaissance 
        {
            get { return _dateNaissance; }
            set { _dateNaissance = value; }
        }
        private string _telp;
        public string Telp
        {
            get { return _telp; }
            set { _telp = value; }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }


        private string _ville;

        public string Ville
        {
            get { return  _ville; }
            set { string _ville = value; }
        }

        private int _codePostale;
        public int CodePostale
        {
            get { return _codePostale; }
            set { _codePostale = value; }
        }

        private string _genre;
        public string Genre
        {
            get { return _genre; }
            set { _genre = value; }
        }

        #endregion

        public Person(string matricule, string nom, string prenom,string address,string datenaissance,string tel,string email, string ville,int cpville,string genre)
        {
            this.Matricule = matricule;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Address = address;
            this.DateNaissance = datenaissance;
            this.Telp = tel;
            this.Email = email;
            this.Ville = ville;
            this.CodePostale = cpville;
            this.Genre = genre;
            
        }

       /* public bool insert ()
        {
            DatabaseConnection db = new DatabaseConnection();
            db.connect();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText = "INSERT INTO CORRESPONDANT(SEQUENCE_ID.NEXTVAL,'" + Matricule + "', '" + Nom + "','"+Prenom+ "' ,'" + Address + "' ,'" + DateNaissance + "' ,'" + Telp + "','" + Email + "' , '" + Ville + "' , " + CodePostale+ "', '" + Genre + "')";
            int rowsUpdated = cmd.ExecuteNonQuery();
            if (rowsUpdated == 0)
            {
                MessageBox.Show("Veuiilez Remplir touts les champs", "Erreur d'ajout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;

            }
            else
            { 
            MessageBox.Show("Ajout Réussi!");
            return true;
            }
            // con.Dispose();


        }
        */
    }
}
