﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CNSSwpf.Correspondant
{
    /// <summary>
    /// Interaction logic for CorrespandontView.xaml
    /// </summary>
    public partial class CorrespandontView : Window
    {
        public CorrespandontView()
        {
            InitializeComponent();
            this.DataContext = new CorrespondantVM();
        }
    }
}
