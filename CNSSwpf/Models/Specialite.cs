﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNSSwpf.Models
{
    class Specialite : Personnel
    {
        private int _idSpecialite;

        public int IdSpecialite
        {
            get { return _idSpecialite; }
            set { _idSpecialite = value; }
        }
        private string _designation;

        public string Designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

    }
}
